#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=5000
HISTFILESIZE=10000

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias ll='ls -alFh'
    alias la='ls -A'
    alias l='ls -lFh'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Disable bell sound
xset -b b off

#
### Aliases
#

# start qdb without splash screen
alias gdb='gdb -q'
alias xc='xclip -selection clipboard'

# pdflatex shell escape default
alias pdflatex='pdflatex -shell-escape'
alias latexclean='rm -r _minted-report *.{aux,log,toc,out}'
alias latexcleanall='rm -r _minted-report *.{aux,log,toc,out,pdf}'

# grep history
alias hg='history | grep'

PS1="\[\e[38;2;128;0;0m\][\u@\h]:\[\e[38;2;225;225;75m\]\w\$\[\e[0m\] "

#
### Exports
#

export EDITOR='vim'
export VISUAL='vim'

# Disable dotnet telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# Home bin path
export PATH=~/bin:$PATH

# Go path
export GOPATH="$HOME/programming/go"
export PATH="$PATH:$HOME/programming/go/bin"
export PATH="$PATH:$HOME/.cargo/bin"

# Java options
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'

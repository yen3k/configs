" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
Plug 'ntpeters/vim-better-whitespace'
Plug 'udalov/kotlin-vim'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'andys8/vim-elm-syntax'
Plug 'supercollider/scvim'
Plug 'tidalcycles/vim-tidal'
Plug 'elixir-editors/vim-elixir'
Plug 'BeneCollyridam/futhark-vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

syntax on
set number
set relativenumber
set colorcolumn=80

" Indentation default
set autoindent
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

" File specific indentation rules
" C#
autocmd FileType cs setlocal ts=4 sts=4 sw=4
" Markdown
let g:pandoc#syntax#conceal#use = 0
autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc |
  \ set virtualedit=all

local opts = {
  tools = { -- rust-tools options
  autoSetHints = true,
  inlay_hints = {
    only_current_line = true,
  },
  server = {
    on_attach = on_attach,
    settings = {
      ["rust-analyzer"] = {
        checkOnSave = {
          command = "clippy",
        },
      },
    },
  },
},
}

require('rust-tools').setup(opts)

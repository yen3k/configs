local map = vim.api.nvim_set_keymap
-- Toggle Nerdtree
map('n', '<C-n>', ':NERDTreeToggle<CR>', { silent = true, noremap = true })

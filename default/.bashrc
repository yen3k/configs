#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1="\[\e[38;2;128;128;0m\][\u@\h]:\[\e[38;2;225;225;75m\]\w\$\[\e[0m\] "

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias ll='ls -alFh'
    alias la='ls -A'
    alias l='ls -lFh'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# aliases
alias hg='history | grep'

# exports
export EDITOR='vim'
export VISUAL='vim'


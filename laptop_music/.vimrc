" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

Plug 'ntpeters/vim-better-whitespace'
" List ends here. Plugins become visible to Vim after this call.
call plug#end()

syntax on
set number
set relativenumber
set colorcolumn=80

" Indentation default
set autoindent
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab
